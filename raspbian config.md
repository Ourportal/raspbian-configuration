# Raspbian Configuration #

### Config Table ###

| PI image version | date      | Landcam | Pi Bridge | OS         | Open CV | FFMPEG |
| ---------------- | --------- | ------- | --------- | ---------- | ------- | ------ |
| 20210517         | 5/17/2021 | 0.2.3   | 0.1.2     | Raspbian 9 | 3.3     | 3.4    |
|                  |           |         |           |            |         |        |
|                  |           |         |           |            |         |        |

## Change Log

### 20210517

* Initial tracking
* Contains:
  * Bridge 0.1.2
  * Landcame 0.2.3